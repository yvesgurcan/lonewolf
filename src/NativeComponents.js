/* React Native equivalents of web HTML tags  */

/*
    Note: These components are not ready for React Native conversion yet. Resolve the TODOs first to export to a Native project.
*/

import React, { Component } from 'react'
import {
    View,
    Text,
    TextInput,
    Button,
} from 'react-native'
import Styles from './StylesNative'

export class Header1 extends Component {
    render() {
        return (
            <Text style={{...Styles.H1}}>{this.props.children}</Text>
        )
    }
}

export class Link extends Component {
    render() {
        return (
            <Text href={this.props.href} target={this.props.target} onClick={this.props.onClick} style={{...Styles.Link}}>{this.props.children}</Text>
        )
    }
}

export class TextWithInputFont extends Component {
    render() {
        return (
            <Text {...this.props} style={{...Styles.TextWithInputFont, ...this.props.style}}/>
        )
    }
}

export class Label extends Component {
    render() {
        return (
            <View style={{...(this.props.noMargin ? null : Styles.LabelContainer)}} hidden={this.props.hidden} onClick={this.props.onClick}>
                <Text style={Styles.Label}>{this.props.children}</Text>
            </View>
        )
    }
}

export class LabelInline extends Component {
    render() {
        return (
            <Text {...this.props} style={{...this.props.style}}>{this.props.children}.</Text>
        )
    }
}

export class TextArea extends Component {
    render() {
        return (
            <TextInput {...this.props}/>
        )
    }
}

export class PickerItemGroup extends Component {
    render() {
        // TODO: not sure exactly how to handle optgroup in React Native
        return <optgroup {...this.props} />
    }
}

export class PickerItem extends Component  {
    render() {
        return <Picker.Item {...this.props} />
    }
}

export class ButtonContainer extends Component {
    render() {
        return (
            <View style={this.props.style}>
                <Button title={this.props.title} onPress={this.props.onClick} addFaceValue={this.props.addFaceValue} />
            </View>
        )
    }
}